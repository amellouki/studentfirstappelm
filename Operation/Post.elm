module Post exposing (Post, PostId, idToString, postDecoder, postsDecoder)
import Json.Decode as Decode exposing (Decoder, int, list, string)
import Json.Decode.Pipeline exposing (required)


-- Declaration of variables 

type alias Post =
    { id : PostId
    , age : String
    , persoName : String
    , studentUrl : String
    }

type PostId
    = PostId Int


-- convert id de type Int to String

idToString : PostId -> String
idToString postId =
    case postId of
        PostId id ->
            String.fromInt id

-- make a list of decoder posts

postsDecoder : Decoder (List Post)
postsDecoder =
    list postDecoder



idDecoder : Decoder PostId
idDecoder =
    Decode.map PostId int

postDecoder : Decoder Post
postDecoder =
    Decode.succeed Post
        |> required "id" idDecoder
        |> required "age" string
        |> required "studentName" string
        |> required "studentUrl" string
        


